Altium Designer Pick and Place Locations
C:\Users\sandy\Dropbox\ALK-Advanced_Lockers\Hardware\Design\Electronics\Development\Bumble_C01 - Door\Project Outputs for Maintenance\Pick Place for BZK_Bumble_C01(Production TTL Camera).txt

========================================================================================================================
File Design Information:

Date:       24/10/20
Time:       08:39
Revision:   Not in VersionControl
Variant:    Production TTL Camera
Units used: mm

Designator Center-X(mm) Center-Y(mm) Layer       Rotation 
V5         -9.2000      -13.3000     BottomLayer 180      
R27        -10.1407     -6.8000      BottomLayer 90       
R26        -8.6407      -6.8000      BottomLayer 270      
C20        -11.5407     -6.8000      BottomLayer 90       
X13        33.6250      0.9000       TopLayer    270      
X12        24.9000      23.7250      TopLayer    360      
X7         -19.4000     23.7250      TopLayer    360      
X4         -31.5750     0.6000       TopLayer    90       
X2         -19.0000     -22.2250     TopLayer    180      
X1         24.8000      -22.2750     TopLayer    180      
V4         11.0000      18.1000      TopLayer    90       
X3         -22.3000     -5.1995      TopLayer    270      
X9         -22.3000     5.2005       TopLayer    270      
V3         -7.7250      -10.0000     TopLayer    90       
U8         -12.3000     -9.0000      TopLayer    180      
U4         -9.6000      1.6000       TopLayer    180      
R21        -13.2000     -5.7000      TopLayer    90       
R22        -11.9000     1.8000       TopLayer    90       
R8         -13.2000     -2.3500      TopLayer    90       
R7         -10.6000     -1.2000      TopLayer    180      
R1         -9.7000      -8.8500      TopLayer    90       
R23        -11.0000     4.4000       TopLayer    360      
L1         -9.0000      -4.6000      TopLayer    180      
C18        -13.3831     1.7901       TopLayer    90       
C14        -7.8500      4.4000       TopLayer    360      
C19        -10.4500     -11.6000     TopLayer    360      
C16        -7.4000      -1.1000      TopLayer    360      
X11        28.2000      17.3400      TopLayer    270      
X6         -26.1000     -15.8600     TopLayer    90       
X8         -25.9000     17.2600      TopLayer    270      
V17        -6.1000      -17.3000     TopLayer    180      
V16        -5.7000      9.1000       TopLayer    180      
V15        11.6926      -18.2004     TopLayer    0        
U7         23.6500      9.8750       TopLayer    90       
U2         20.7500      -6.0500      TopLayer    180      
R37        5.3000       15.3000      TopLayer    270      
R34        25.0000      4.7000       TopLayer    360      
R33        23.6000      12.4000      TopLayer    180      
R32        24.5030      7.1000       TopLayer    360      
R31        -11.7000     -15.2000     TopLayer    90       
R29        -18.8890     -11.2000     TopLayer    180      
R24        -18.9110     -9.8000      TopLayer    180      
R20        15.7000      -14.0000     TopLayer    270      
R15        27.3000      -5.3500      TopLayer    90       
R9         29.3500      -11.3000     TopLayer    360      
P1         29.2300      -14.3650     TopLayer    180      
G1         30.2152      -5.2086      TopLayer    360      
DS2        4.4500       -9.7375      TopLayer    360      
DS1        4.4500       -11.7125     TopLayer    360      
C7         -11.7000     -20.3000     TopLayer    180      
C6         13.4000      -14.7000     TopLayer    180      
C5         30.7000      -2.8000      TopLayer    0        
C4         30.7000      -7.6000      TopLayer    180      
C3         27.6000      -2.8000      TopLayer    0        
C2         29.3286      -10.0053     TopLayer    180      
C1         16.6000      0.2000       TopLayer    180      
R6         11.7000      7.4000       TopLayer    90       
R3         27.6000      -1.4000      TopLayer    180      
C17        1.2000       3.4000       TopLayer    90       
C24        -11.6475     7.3950       TopLayer    180      
X10        -13.3000     17.3400      TopLayer    270      
X5         4.5000       -18.3700     TopLayer    360      
X14        0.0500       0.6500       BottomLayer 360      
V13        3.5000       20.8000      TopLayer    180      
V12        13.5890      -11.0000     TopLayer    180      
V10        -0.1870      17.1160      TopLayer    90       
V9         -2.3000      4.2000       TopLayer    360      
V8         0.2000       -18.7000     TopLayer    270      
U3         1.4549       -10.7892     TopLayer    270      
U1         2.7000       -5.6000      TopLayer    360      
S1         29.7000      -19.0000     TopLayer    360      
R19        7.5000       22.0395      TopLayer    360      
R18        25.8500      -11.3000     TopLayer    360      
R17        13.3500      -13.1500     TopLayer    360      
R16        3.3000       15.4970      TopLayer    180      
R14        -16.9000     0.0000       TopLayer    360      
R13        -3.8460      -6.6985      TopLayer    360      
R12        -3.9000      -11.1000     TopLayer    360      
R11        -3.9000      -8.2000      TopLayer    180      
R10        -3.9000      -9.6000      TopLayer    180      
R5         7.6000       -10.6000     TopLayer    90       
L4         11.0000      13.2000      TopLayer    180      
L3         2.2000       -18.7000     TopLayer    90       
L2         -2.6000      2.3000       TopLayer    180      
C13        8.8000       -5.2000      TopLayer    90       
C10        26.3000      9.8000       TopLayer    270      
C9         -2.7000      -4.3000      TopLayer    90       
C8         -4.4000      -4.3000      TopLayer    90       
R2         -4.2869      14.0413      TopLayer    360      
R4         18.3718      5.0000       TopLayer    90       
V2         15.0000      17.9474      TopLayer    270      
V7         -4.1680      17.1680      TopLayer    360      
V14        14.8000      4.8000       TopLayer    90       
X31        15.2000      9.8000       TopLayer    360      
